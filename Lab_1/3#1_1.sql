CREATE DATABASE NewDatabase;

USE NewDatabase;
GO

CREATE SCHEMA sales;
GO

CREATE SCHEMA persons;
GO

CREATE TABLE sales.Orders
(
    OrderNum INT NULL
);

BACKUP DATABASE NewDatabase
    TO DISK = 'D:\VladislavKrokh.bak'
GO

DROP DATABASE NewDatabase

RESTORE DATABASE NewDatabase FROM DISK = 'D:\VladislavKrokh.bak'