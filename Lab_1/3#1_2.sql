SELECT DepartmentID, Name
FROM HumanResources.Department
WHERE Name like 'P%'

SELECT BusinessEntityID, JobTitle, Gender, VacationHours, SickLeaveHours
FROM HumanResources.Employee
WHERE VacationHours BETWEEN 10 AND 13

SELECT BusinessEntityID, JobTitle, Gender, BirthDate, HireDate
FROM HumanResources.Employee
where DAY(HireDate) = 01
  and MONTH(HireDate) = 07
ORDER BY BusinessEntityID
    OFFSET 3 ROWS
    FETCH FIRST 5 ROWS ONLY