CREATE DATABASE VladislavKrokh;

USE VladislavKrokh;
GO

CREATE SCHEMA sales;
GO

CREATE SCHEMA persons;
GO

CREATE TABLE sales.Orders
(
  OrderNum INT NULL
);

BACKUP DATABASE VladislavKrokh
TO DISK = 'D:\VladislavKrokh.bak'
GO

DROP DATABASE VladislavKrokh

RESTORE DATABASE VladislavKrokh FROM DISK = 'D:\VladislavKrokh.bak'