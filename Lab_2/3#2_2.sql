create table dbo.Address
(
    AddressID       int          NOT NULL,
    AddressLine1    nvarchar(60) NOT NULL,
    AddressLine2    nvarchar(60) NULL,
    City            nvarchar(30) NOT NULL,
    StateProvinceID int          NOT NULL,
    PostalCode      nvarchar(15) NOT NULL,
    ModifiedDate    datetime     NOT NULL
);

ALTER TABLE dbo.Address
    ADD CONSTRAINT PK_StateProvinceID_PostalCode PRIMARY KEY (StateProvinceID, PostalCode);

ALTER TABLE dbo.Address
    ADD CONSTRAINT CHK_PostalCode CHECK (PostalCode NOT LIKE '%[^0-9]%');

ALTER TABLE dbo.Address
    ADD CONSTRAINT DF_ModifiedDate DEFAULT GETDATE() FOR ModifiedDate;

INSERT INTO dbo.Address
SELECT AddressID, AddressLine1, AddressLine2, City, StateProvinceID, PostalCode, ModifiedDate
FROM (
         SELECT AddressID,
                AddressLine1,
                AddressLine2,
                City,
                p_address.StateProvinceID,
                PostalCode,
                p_address.ModifiedDate,
                ROW_NUMBER()
                        OVER (PARTITION BY p_address.StateProvinceID, PostalCode ORDER BY p_address.AddressID DESC) AS rn
         FROM Person.Address p_address
                  INNER JOIN Person.StateProvince province on province.StateProvinceID = p_address.StateProvinceID
         WHERE province.CountryRegionCode = 'US'
           and p_address.PostalCode NOT LIKE '%[^0-9]%'
     ) as a
where a.rn = 1

ALTER TABLE dbo.Address
    ALTER COLUMN City NVARCHAR(20) NOT NULL;