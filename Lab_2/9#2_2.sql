CREATE TABLE dbo.StateProvince
(
  StateProvinceID         INT          NOT NULL,
  StateProvinceCode       NVARCHAR(3)  NOT NULL,
  CountryRegionCode       NVARCHAR(3)  NULL,
  IsOnlyStateProvinceFlag BIT          NOT NULL,
  Name                    NVARCHAR(50) NOT NULL,
  TerritoryID             NVARCHAR(15) NOT NULL,
  ModifiedDate            DATETIME     NOT NULL
);

ALTER TABLE dbo.StateProvince
  ADD CONSTRAINT PK_StateProvinceID_StatePostalCode PRIMARY KEY (StateProvinceID, StateProvinceCode);

ALTER TABLE dbo.StateProvince
  ADD CONSTRAINT CHK_TerritoryID CHECK (TerritoryID % 2 = 0);

ALTER TABLE dbo.StateProvince
  ADD CONSTRAINT DF_TerritoryID DEFAULT 2 FOR TerritoryID;

INSERT INTO
  dbo.StateProvince (StateProvinceID,
                     StateProvinceCode,
                     CountryRegionCode,
                     IsOnlyStateProvinceFlag,
                     Name,
                     ModifiedDate)
  SELECT
    StateProvinceID,
    StateProvinceCode,
    CountryRegionCode,
    IsOnlyStateProvinceFlag,
    Name,
    ModifiedDate
  FROM (
         SELECT
           p_address.StateProvinceID,
           StateProvinceCode,
           CountryRegionCode,
           IsOnlyStateProvinceFlag,
           p_address.Name,
           p_address.TerritoryID,
           p_address.ModifiedDate,
           ROW_NUMBER()
           OVER ( PARTITION BY p_address.StateProvinceID, StateProvinceCode
             ORDER BY address.AddressID DESC ) AS rn
         FROM Person.StateProvince p_address
           INNER JOIN Person.Address address ON address.StateProvinceID = p_address.StateProvinceID
           INNER JOIN Person.BusinessEntityAddress business_address ON business_address.AddressID = address.AddressID
           INNER JOIN Person.AddressType address_type ON address_type.AddressTypeID = business_address.AddressTypeID
         WHERE address_type.Name = 'Shipping'
       ) AS a
  WHERE a.rn = 1

ALTER TABLE dbo.StateProvince
  ALTER COLUMN IsOnlyStateProvinceFlag SMALLINT NULL;