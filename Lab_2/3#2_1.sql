SELECT employees.BusinessEntityID, employees.JobTitle, department.DepartmentID, department.Name
FROM HumanResources.Employee employees
         INNER JOIN HumanResources.EmployeeDepartmentHistory employees_dep
                    on employees.BusinessEntityID = employees_dep.BusinessEntityID
         INNER JOIN HumanResources.Department department ON employees_dep.DepartmentID = department.DepartmentID
WHERE employees_dep.EndDate IS NULL

SELECT departments.DepartmentID, departments.Name, COUNT(employees_dep.DepartmentID) AS EmpCount
FROM HumanResources.Department departments
         INNER JOIN HumanResources.EmployeeDepartmentHistory employees_dep
                    ON employees_dep.DepartmentID = departments.DepartmentID
WHERE employees_dep.EndDate IS NULL
GROUP BY departments.DepartmentID, departments.Name

SELECT employees.JobTitle,
       employees_pay.Rate,
       employees_pay.RateChangeDate,
       FORMATMESSAGE('The rate for %s was set to %s at %s', employees.JobTitle,
                     CONVERT(varchar(max), employees_pay.Rate),
                     CONVERT(varchar(max), employees_pay.RateChangeDate, 106)) as Report
FROM HumanResources.Employee employees
         INNER JOIN HumanResources.EmployeePayHistory employees_pay
                    on employees.BusinessEntityID = employees_pay.BusinessEntityID


