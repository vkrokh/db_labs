SELECT
  employees.BusinessEntityID,
  employees.JobTitle,
  AVG(ph.Rate) as AvarageRate
FROM HumanResources.Employee employees
  LEFT JOIN HumanResources.EmployeePayHistory ph
    ON employees.BusinessEntityID = ph.BusinessEntityID
GROUP BY employees.BusinessEntityID, employees.JobTitle

SELECT
  employees.BusinessEntityID,
  employees.JobTitle,
  ph.Rate,
  CASE 
	  WHEN ph.Rate <=50 THEN 'less or equal 50'
	  WHEN ph.Rate > 100 THEN 'more than 100'
	  ELSE 'more than 50 but less or equal 100'
	END as RateReport
FROM HumanResources.Employee employees
  LEFT JOIN HumanResources.EmployeePayHistory ph
    ON employees.BusinessEntityID = ph.BusinessEntityID
GROUP BY employees.BusinessEntityID, employees.JobTitle, ph.Rate

SELECT
   departments.DepartmentID,
  departments.Name,
  MAX(pay_histiry.Rate) AS MaxRate
FROM HumanResources.Department departments
  INNER JOIN HumanResources.EmployeeDepartmentHistory employees_dep
    ON employees_dep.DepartmentID = departments.DepartmentID
  INNER JOIN HumanResources.EmployeePayHistory pay_histiry
    ON pay_histiry.BusinessEntityID = employees_dep.BusinessEntityID
WHERE employees_dep.EndDate IS NULL and pay_histiry.Rate > 60
GROUP BY  departments.Name, departments.DepartmentID
ORDER BY MaxRate
